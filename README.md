#Nations of War
####Nations of war is a free-to-play multiplayer strategy game that allows you to create your own "Outpost", and then go to battle with your Units.

#####Developer Guidelines
1. 4 space indentation is to be used
2. If a statement is on one line, then this format should be used:
```java
if(true)
    System.out.println("Look mum, no brackets!");
else
    System.out.println("Goodbye, cruel world ):");
```