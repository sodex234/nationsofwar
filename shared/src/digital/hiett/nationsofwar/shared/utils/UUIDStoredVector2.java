package digital.hiett.nationsofwar.shared.utils;

import java.util.UUID;

public class UUIDStoredVector2 extends SimpleVector2 {

    private UUID uuid;

    public UUIDStoredVector2(int x, int y, UUID uuid) {
        super(x, y);
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

}
