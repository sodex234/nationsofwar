package digital.hiett.nationsofwar.shared.events.managers;

import java.util.ArrayList;
import java.util.List;

public class EventManager {

    private List<Event> events = new ArrayList<>();

    public List<Event> getEvents() {
        return events;
    }

    public void registerEvent(Event event) {
        this.events.add(event);
    }

}
