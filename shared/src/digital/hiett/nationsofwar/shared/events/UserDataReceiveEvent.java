package digital.hiett.nationsofwar.shared.events;

import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.packets.ReadPacket;

import java.util.UUID;

public abstract class UserDataReceiveEvent implements Event {

    @Override
    public void handleEvent(ReadPacket readPacket) {
        onEventCalled(UUID.fromString(readPacket.getData()[0]), readPacket.getData()[1]);
    }

    public abstract void onEventCalled(UUID uuid, String username);

}
