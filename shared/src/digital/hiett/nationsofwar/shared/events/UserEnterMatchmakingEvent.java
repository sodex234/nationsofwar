package digital.hiett.nationsofwar.shared.events;

import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.packets.ReadPacket;

import java.util.UUID;

public abstract class UserEnterMatchmakingEvent implements Event {

    @Override
    public void handleEvent(ReadPacket readPacket) {
        onEventReceive(readPacket.getSender(), readPacket.getData()[0]);
    }

    public abstract void onEventReceive(UUID sender, String queueType);

}
