package digital.hiett.nationsofwar.shared.events;

import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.packets.ReadPacket;

public abstract class DataReceiveEvent implements Event {

    @Override
    public void handleEvent(ReadPacket readPacket) {
        onEventCalled(readPacket);
    }

    public abstract void onEventCalled(ReadPacket packet);

}
