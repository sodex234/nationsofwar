package digital.hiett.nationsofwar.shared.packets;

public enum PacketDirection {

    IN, OUT

}
