package digital.hiett.nationsofwar.shared.packets;

import java.util.UUID;

public class ReadPacket {

    private Packet packet;
    private String[] data;
    private UUID sender;

    public ReadPacket(Packet packet, String[] data, UUID sender) {
        this.packet = packet;
        this.data = data;
        this.sender = sender;
    }

    public ReadPacket(Packet packet, String[] data) {
        this(packet, data, null);
    }

    public void setSender(UUID sender) {
        this.sender = sender;
    }

    public UUID getSender() {
        return sender;
    }

    public boolean isFromServer() {
        return sender == null;
    }

    public Packet getPacket() {
        return packet;
    }

    public String[] getData() {
        return data;
    }

}
