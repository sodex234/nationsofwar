package digital.hiett.nationsofwar.shared.packets;

import digital.hiett.nationsofwar.shared.events.*;
import digital.hiett.nationsofwar.shared.events.managers.Event;

import java.util.Arrays;

public enum Packet {

    // Packet direction assumes this is the server.

    USER_INFO(PacketDirection.OUT, 0, UserDataReceiveEvent.class),
    ENTER_MATCHMAKING(PacketDirection.IN, 1, UserEnterMatchmakingEvent.class),
    QUEUE_SIZE(PacketDirection.OUT, 2, GeneralQueueSizeEvent.class),
    PLAYER_PLACE_UNIT(PacketDirection.IN, 3, PlayerPlaceUnitEvent.class),
    OTHER_PLAYER_PLACE_UNIT(PacketDirection.OUT, 4, OtherPlayerPlaceUnitEvent.class),
    PLAYER_ATTACK(PacketDirection.IN, 5, PlayerAttackEvent.class),
    OTHER_PLAYER_ATTACK(PacketDirection.OUT, 6, OtherPlayerAttackEvent.class),
    OTHER_PLAYER_INFO(PacketDirection.OUT, 7, OtherPlayerInfoEvent.class),
    SET_PLAYER_GO(PacketDirection.OUT, 8, SetPlayerGoEvent.class),
    PLAYER_SELECTION(PacketDirection.IN, 9, PlayerSelectionEvent.class),
    THIS_PLAYER_DELETE(PacketDirection.OUT, 10, ThisPlayerDelete.class),
    GAME_OVER(PacketDirection.IN, 11, GameOverEvent.class);

    private PacketDirection direction;
    private int id;
    private Class<? extends Event>[] events;

    Packet(PacketDirection direction, int id, Class<? extends Event>... events) {
        this.id = id;
        this.direction = direction;
        this.events = events;
    }

    public Class<? extends Event>[] getEvents() {
        return events;
    }

    public PacketDirection getDirection(boolean isServer) {
        return isServer ? direction : oppositeDirection();
    }

    public int getId() {
        return id;
    }

    private PacketDirection oppositeDirection() {
        return direction == PacketDirection.OUT ? PacketDirection.IN : PacketDirection.OUT;
    }

    public String build(int... data) {
        String[] strArray = new String[data.length];

        int cnt = 0;
        for (int i : data) {
            strArray[cnt] = "" + i;

            cnt++;
        }

        return build(strArray);
    }

    public String build(String... data) {
        StringBuilder stringBuilder = new StringBuilder(this.getId() + ":");

        Arrays.asList(data).forEach(dataItem -> stringBuilder.append(dataItem + ":")); // I know this is against the point of StringBuilder, but I'm doing it anyway.

        String out = stringBuilder.toString();
        return out.substring(0, out.length() - 1);
    }

    // Static methods

    public static Packet fromId(int id) {
        for (Packet p : values())
            if (p.getId() == id)
                return p;

        return null;
    }


    public static ReadPacket processPacket(String packet) {
        String[] packetParts = packet.split(":");

        Packet p = Packet.fromId(Integer.parseInt(packetParts[0]));

        String[] withoutFirst = new String[packetParts.length - 1];
        int countId = 0;
        for (String s : packetParts) {
            countId++;

            if (countId == 1)
                continue;

            withoutFirst[countId - 2] = s;
        }

        System.out.println("Processed packed with the type of " + p + " and the array of " + Arrays.asList(withoutFirst).toString());

        return new ReadPacket(p, withoutFirst);
    }

}
