package digital.hiett.nationsofwar.server.instances;

public enum BattleMatchState {

    DROP_UNITS, INGAME, ENDING;

}
