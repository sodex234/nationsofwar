package digital.hiett.nationsofwar.server.instances;

import digital.hiett.nationsofwar.server.Main;
import digital.hiett.nationsofwar.server.users.User;
import digital.hiett.nationsofwar.shared.packets.Packet;
import digital.hiett.nationsofwar.shared.utils.UUIDStoredVector2;

import java.util.UUID;

public class BattleMatch {

    private UUID playerA, playerB;
    private BattleMatchState battleMatchState;
    private int[][] aSquare, bSquare;
    private boolean aTurn, ended = false;
    private UUIDStoredVector2 selection1, selection2;

    public BattleMatch(UUID playerA, UUID playerB) {
        this.playerA = playerA;
        this.playerB = playerB;

        this.aTurn = true;

        this.aSquare = new int[50][50];
        this.bSquare = new int[50][50];

        this.battleMatchState = BattleMatchState.DROP_UNITS;

        // Send packets out to these players telling them they're in a game.
        getPlayerA().sendPacket(Packet.OTHER_PLAYER_INFO.build(playerB + "", getPlayerB().getName()));
        getPlayerB().sendPacket(Packet.OTHER_PLAYER_INFO.build(playerA + "", getPlayerA().getName()));

        getPlayerA().sendPacket(Packet.SET_PLAYER_GO.build("true"));
        getPlayerB().sendPacket(Packet.SET_PLAYER_GO.build("false"));
    }

    public UUIDStoredVector2 getSelection1() {
        return selection1;
    }

    public UUIDStoredVector2 getSelection2() {
        return selection2;
    }

    public void setSelection1(UUIDStoredVector2 selection1) {
        this.selection1 = selection1;
    }

    public void setSelection2(UUIDStoredVector2 selection2) {
        this.selection2 = selection2;

        // The second selection has been set. Fire the attack.
        setNoGo();

        // Fire the attack!
        if(aTurn) {
            getPlayerA().sendPacket(Packet.THIS_PLAYER_DELETE.build("true"));
        } else {
            getPlayerB().sendPacket(Packet.THIS_PLAYER_DELETE.build("true"));
        }

        sendBoth(Packet.OTHER_PLAYER_ATTACK.build(selection1.getUuid() + "", selection2.getUuid() + ""));

        // Tell the next player that its their go.
        aTurn = !aTurn;

        this.selection1 = null;
        this.selection2 = null;

        updatePlayerGo();
        sendBoth(Packet.THIS_PLAYER_DELETE.build("false"));
    }

    public boolean isATurn() {
        return aTurn;
    }

    public boolean isBTurn() {
        return !aTurn;
    }

    public int[][] getASquare() {
        return aSquare;
    }

    public int[][] getBSquare() {
        return bSquare;
    }

    public BattleMatchState getBattleMatchState() {
        return battleMatchState;
    }

    public void setBattleMatchState(BattleMatchState battleMatchState) {
        this.battleMatchState = battleMatchState;
    }

    public User getPlayerA() {
        return Main.getInstance().getUserManager().get(playerA);
    }

    public User getPlayerB() {
        return Main.getInstance().getUserManager().get(playerB);
    }

    public void sendBoth(String packet) {
        getPlayerB().sendPacket(packet);
        getPlayerA().sendPacket(packet);
    }

    private void setNoGo() {
        sendBoth(Packet.SET_PLAYER_GO.build("false"));
    }

    private void updatePlayerGo() {
        if (isATurn()) {
            getPlayerA().sendPacket(Packet.SET_PLAYER_GO.build("true"));
            getPlayerB().sendPacket(Packet.SET_PLAYER_GO.build("false"));
        } else {
            getPlayerA().sendPacket(Packet.SET_PLAYER_GO.build("false"));
            getPlayerB().sendPacket(Packet.SET_PLAYER_GO.build("true"));
        }
    }

    public void endMatch() {
        if(ended)
            return;

        ended = true;

        //TODO: Confirm this with clients.

        System.out.println("The game has ended.");
        // This game has ended so remove this item from the array
        Main.getInstance().getMatchmakingManager().removeMatch(this);
    }

}
