package digital.hiett.nationsofwar.server.listeners;

import digital.hiett.nationsofwar.server.Main;
import digital.hiett.nationsofwar.shared.events.GameOverEvent;

import java.util.UUID;

public class OnGameOver extends GameOverEvent {

    @Override
    public void onEventCalled(UUID sender) {
        // Check what match they're in
        if(Main.getInstance().getMatchmakingManager().inGame(sender)) // The game could already be deleted.
            Main.getInstance().getMatchmakingManager().getGame(sender).endMatch();
    }

}
