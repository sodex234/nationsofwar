package digital.hiett.nationsofwar.server.listeners;

import digital.hiett.nationsofwar.server.Main;
import digital.hiett.nationsofwar.server.instances.BattleMatch;
import digital.hiett.nationsofwar.shared.events.PlayerPlaceUnitEvent;
import digital.hiett.nationsofwar.shared.packets.Packet;

import java.util.UUID;

public class OnPlayerPlaceUnit extends PlayerPlaceUnitEvent {

    @Override
    public void onEventCalled(UUID sender, int unitId, int x, int y, UUID uuid) {
        // Find the game that this player is in
        BattleMatch battleMatch = Main.getInstance().getMatchmakingManager().getGame(sender);

        if (battleMatch == null) // Just in case...
            return;

        // Set the state and tell the other player what has happened
        if (battleMatch.getPlayerA().getUuid().equals(sender)) {
            // Player A
            battleMatch.getASquare()[x][y] = unitId;
            battleMatch.getPlayerB().sendPacket(Packet.OTHER_PLAYER_PLACE_UNIT.build(unitId + "", x + "", y + "", uuid + ""));
        } else {
            // Player B
            battleMatch.getBSquare()[x][y] = unitId;
            battleMatch.getPlayerA().sendPacket(Packet.OTHER_PLAYER_PLACE_UNIT.build(unitId + "", x + "", y + "", uuid + ""));
        }
    }

}