package digital.hiett.nationsofwar.server.listeners;

import digital.hiett.nationsofwar.server.Main;
import digital.hiett.nationsofwar.server.instances.BattleMatch;
import digital.hiett.nationsofwar.shared.events.PlayerSelectionEvent;
import digital.hiett.nationsofwar.shared.utils.UUIDStoredVector2;

import java.util.UUID;

public class OnPlayerSelection extends PlayerSelectionEvent {

    @Override
    public void onEventCalled(UUID sender, int x, int y, UUID uuid) {
        BattleMatch battleMatch = Main.getInstance().getMatchmakingManager().getGame(sender);

        if (battleMatch.getSelection1() == null)
            battleMatch.setSelection1(new UUIDStoredVector2(x, y, uuid));
        else
            battleMatch.setSelection2(new UUIDStoredVector2(x, y, uuid));
    }

}
