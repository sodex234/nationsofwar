package digital.hiett.nationsofwar.server.users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class UserManager {

    private HashMap<UUID, User> users = new HashMap<>();

    public void add(User user) {
        this.users.put(user.getUuid(), user);
    }

    public void remove(UUID uuid) {
        this.users.remove(uuid);
    }

    public boolean contains(UUID uuid) {
        return this.users.containsKey(uuid);
    }

    public User get(UUID uuid) {
        return this.users.get(uuid);
    }

    public List<User> toArray() {
        return new ArrayList<>(users.values());
    }

}
