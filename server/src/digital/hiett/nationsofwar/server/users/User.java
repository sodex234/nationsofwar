package digital.hiett.nationsofwar.server.users;

import digital.hiett.nationsofwar.server.Main;
import digital.hiett.nationsofwar.server.users.stats.Stat;
import digital.hiett.nationsofwar.shared.events.managers.Event;
import digital.hiett.nationsofwar.shared.packets.Packet;
import digital.hiett.nationsofwar.shared.packets.ReadPacket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class User {

    private UUID uuid;
    private String name;
    private Socket socket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    private Thread thread;
    private Map<Stat, Integer> userStats;

    public User(Socket socket) throws IOException {
        this.uuid = UUID.randomUUID();
        this.name = "ALPHA PLAYER";
        this.socket = socket;

        this.dataOutputStream = new DataOutputStream(this.socket.getOutputStream());
        this.dataInputStream = new DataInputStream(this.socket.getInputStream());

        this.thread = new Thread(() -> {
            while (!isConnected()) {
                try {
                    Thread.sleep(1000 / 60);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // Waiting for connection to be completed
            }

            System.out.println("Connected to server.");
            sendPacket(Packet.USER_INFO.build(this.uuid + "", this.name));

            try {
                while (true) {
                    System.out.println("Waiting on data...");
                    String data = dataInputStream.readUTF();
                    System.out.println(data);

                    ReadPacket readPacket = processPacket(data);

                    // Send the packet to the event managers.
                    for (Event e : Main.getInstance().getEventManager().getEvents())
                        for (Class<? extends Event> e1 : readPacket.getPacket().getEvents())
                            if (e1.isAssignableFrom(e.getClass()))
                                e.handleEvent(readPacket);
                }
            } catch (IOException e) {
                // They have disconnected
                System.out.println("Client disconnected.");

                System.out.println("This uuid is " + this.uuid);
                System.out.println("User Manager is " + Main.getInstance().getUserManager());

                // TODO: Check if they are in a match and remove them from it, then remove the other player and delete it!

                Main.getInstance().getUserManager().remove(this.uuid);
            }
        });

        userStats = new HashMap<>();

        this.thread.start();
    }

    public String getName() {
        return name;
    }

    public ReadPacket processPacket(String data) {
        ReadPacket packet = Packet.processPacket(data);
        packet.setSender(this.uuid);

        return packet;
    }

    public Socket getSocket() {
        return socket;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Thread getThread() {
        return thread;
    }

    public boolean isConnected() {
        return socket.isConnected();
    }

    public void sendPacket(String packet) {
        try {
            System.out.println("Sending packet: " + packet);
            dataOutputStream.writeUTF(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getStat(Stat stat) {
        return userStats.getOrDefault(stat, 0);
    }

    public void incrementStat(Stat stat, int amount) {
        userStats.put(stat, userStats.get(stat) + amount);
    }

    public void incrementStat(Stat stat) {
        incrementStat(stat, 1);
    }

}
