package digital.hiett.nationsofwar.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class TextFieldSkin {

    private TextField.TextFieldStyle skin;

    public TextFieldSkin() {
        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle();
        textFieldStyle.fontColor = Color.WHITE;

        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.BLACK);
        pixmap.fill();

        textFieldStyle.background = new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)));
        textFieldStyle.font = new BitmapFont(Gdx.files.internal("main.fnt"));

        this.skin = textFieldStyle;
    }

    public TextField.TextFieldStyle getStyle() {
        return skin;
    }

}
