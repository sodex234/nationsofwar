package digital.hiett.nationsofwar.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class ImageTextSkin {

    private Skin skin;

    public ImageTextSkin(String texUrl) {
        this(texUrl, Color.WHITE);
    }

    public ImageTextSkin(String texUrl, Color color) {
        skin = new Skin();

        Texture transparent = new Texture(Gdx.files.internal(texUrl));

        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("white", new Texture(pixmap));

        BitmapFont bitmapFont = new BitmapFont(Gdx.files.internal("main.fnt"));
        bitmapFont.setColor(color);
        bitmapFont.getColor().set(0, 0, 1, 1);

        skin.add("default", bitmapFont);

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = new TextureRegionDrawable(new TextureRegion(transparent));
        textButtonStyle.down = new TextureRegionDrawable(new TextureRegion(transparent));
        textButtonStyle.checked = skin.newDrawable("white", Color.BLUE);
        textButtonStyle.over = new TextureRegionDrawable(new TextureRegion(transparent));
        textButtonStyle.font = skin.getFont("default");
        skin.add("default", textButtonStyle);
    }

    public Skin getSkin() {
        return skin;
    }

}
