package digital.hiett.nationsofwar.networking;

import java.util.UUID;

public class GameManager {

    private String otherName;
    private UUID otherUUID;
    private boolean thisPlayersTurn, delete;

    public GameManager() {
        this.otherName = "";
        this.otherUUID = UUID.randomUUID();
        this.thisPlayersTurn = false;
        this.delete = false;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean isThisPlayersTurn() {
        return thisPlayersTurn;
    }

    public void setThisPlayersTurn(boolean thisPlayersTurn) {
        this.thisPlayersTurn = thisPlayersTurn;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public UUID getOtherUUID() {
        return otherUUID;
    }

    public void setOtherUUID(UUID otherUUID) {
        this.otherUUID = otherUUID;
    }

}
