package digital.hiett.nationsofwar.networking.listeners;

import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.shared.events.ThisPlayerDelete;

public class OnThisPlayerDelete extends ThisPlayerDelete {

    @Override
    public void onEventCalled(boolean isThis) {
        Main.getInstance().getGameManager().setDelete(isThis);
    }

}
