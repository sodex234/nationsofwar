package digital.hiett.nationsofwar.networking.listeners;

import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.shared.events.SetPlayerGoEvent;

public class OnSetPlayerGo extends SetPlayerGoEvent {

    @Override
    public void onEventCalled(boolean isGo) {
        Main.getInstance().getGameManager().setThisPlayersTurn(isGo);
    }

}
