package digital.hiett.nationsofwar.networking.listeners;

import com.badlogic.gdx.math.Vector2;
import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.entities.DeployedUnit;
import digital.hiett.nationsofwar.screens.GameScreen;
import digital.hiett.nationsofwar.shared.events.OtherPlayerAttackEvent;

import java.util.UUID;

public class OnOtherPlayerAttack extends OtherPlayerAttackEvent {

    @Override
    public void onEventCalled(UUID from, UUID to) {
        boolean turn = Main.getInstance().getGameManager().isDelete();

        if (!(Main.getInstance().getScreen() instanceof GameScreen))
            return;

        GameScreen gameScreen = (GameScreen) Main.getInstance().getScreen();

        DeployedUnit fromUnit = gameScreen.getUnitFromUUID(from);
        DeployedUnit toUnit = gameScreen.getUnitFromUUID(to);

        Vector2 fromLoc = fromUnit.getLocation();
        Vector2 toLoc = toUnit.getLocation();

        // Damage whatever has just been hit.
        toUnit.damage(fromUnit.getUnitClass().getDamage());

        System.out.println("Unit is dead? " + toUnit.isDead());

        if(toUnit.isDead()) {
            // They've died, remove them from the array.
            System.out.println("Is turn? " + turn);

            gameScreen.removeUnit(to);

//            if(turn)
//                gameScreen.removeTeamUnit(toUnit);
//            else
//                gameScreen.removeEnemyDeployedUnit((int) toUnit.getLocation().x, (int) toUnit.getLocation().y);
        }

        gameScreen.attack((int) fromLoc.x, (int) fromLoc.y, (int) toLoc.x, (int) toLoc.y);
    }

}
