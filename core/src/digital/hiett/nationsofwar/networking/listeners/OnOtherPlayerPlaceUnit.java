package digital.hiett.nationsofwar.networking.listeners;

import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.screens.GameScreen;
import digital.hiett.nationsofwar.shared.events.OtherPlayerPlaceUnitEvent;
import digital.hiett.nationsofwar.shared.utils.SimpleVector2;
import digital.hiett.nationsofwar.shared.utils.Vector2Utils;

import java.util.UUID;

public class OnOtherPlayerPlaceUnit extends OtherPlayerPlaceUnitEvent {

    @Override
    public void onEventCalled(int unitId, int x, int y, UUID uuid) {
        if (!(Main.getInstance().getScreen() instanceof GameScreen))
            return; // Just to be safe...

        GameScreen gameScreen = (GameScreen) Main.getInstance().getScreen();

        // Calculate the 'flipped' position of this location.

        SimpleVector2 correctLoc = Vector2Utils.enemyDataLocToGraphicsLoc(x, y);
        x = correctLoc.x;
        y = correctLoc.y;

        // Set the data.
        if (unitId != -1)
            gameScreen.addEnemyDeployedUnit(unitId, x, y, uuid);
        else
            gameScreen.removeEnemyDeployedUnit(x, y);
    }

}
