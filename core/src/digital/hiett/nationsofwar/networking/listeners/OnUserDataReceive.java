package digital.hiett.nationsofwar.networking.listeners;

import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.shared.events.UserDataReceiveEvent;

import java.util.UUID;

// UserDataReceiveEvent is the event that extends the raw Event class to process the data, and sort it into native type
// Fields.
public class OnUserDataReceive extends UserDataReceiveEvent {

    @Override
    public void onEventCalled(UUID uuid, String username) {
        Main.getInstance().getLocalPlayer().setUuid(uuid);
        Main.getInstance().getLocalPlayer().setName(username);

        System.out.println("Updated the local user data.");
    }

}
