package digital.hiett.nationsofwar.networking.listeners;

import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.shared.events.GeneralQueueSizeEvent;

public class OnGeneralQueueSize extends GeneralQueueSizeEvent {

    @Override
    public void onEventCalled(int queueSize) {
        Main.getInstance().getLocalPlayer().setQueueSize(queueSize);
    }

}
