package digital.hiett.nationsofwar.networking.listeners;

import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.shared.events.OtherPlayerInfoEvent;

import java.util.UUID;

public class OnOtherPlayerInfo extends OtherPlayerInfoEvent {

    @Override
    public void onEventCalled(UUID uuid, String name) {
        System.out.println("Other client connected: Good luck in battle!");
        System.out.println("Your opponents name is " + name + " and their uuid is " + uuid + ".");

        Main.getInstance().getGameManager().setOtherName(name);
        Main.getInstance().getGameManager().setOtherUUID(uuid);

        // Change to the game screen
        Main.getInstance().setShouldChangeToGame(true);
    }

}
