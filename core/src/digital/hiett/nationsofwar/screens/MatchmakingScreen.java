package digital.hiett.nationsofwar.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.util.ImageTextSkin;

public class MatchmakingScreen implements Screen {

    private Stage stage;
    private SpriteBatch spriteBatch;

    public MatchmakingScreen() {
        spriteBatch = new SpriteBatch();
        stage = new Stage();

        Skin skin = new ImageTextSkin("buttons/button.png").getSkin();

        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        Label label = new Label("Finding you a partner...", new Label.LabelStyle(new BitmapFont(Gdx.files.internal("main.fnt")), Color.BLACK));
        table.add(label);

        table.row();

        Label spacer = new Label(" ", new Label.LabelStyle(new BitmapFont(Gdx.files.internal("main.fnt")), Color.BLACK));
        table.add(spacer);

        table.row();

        TextButton exit = new TextButton("Back", skin);
        exit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Main.getInstance().setScreen(new OutpostScreen());
            }
        });
        table.add(exit).spaceBottom(10);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();

        if (Main.getInstance().shouldChangeToGame()) {
            Main.getInstance().setShouldChangeToGame(false);

            Main.getInstance().setScreen(new GameScreen());
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}