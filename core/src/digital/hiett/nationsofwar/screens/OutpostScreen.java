package digital.hiett.nationsofwar.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.outpost.OutpostCurrency;
import digital.hiett.nationsofwar.outpost.OutpostMap;
import digital.hiett.nationsofwar.outpost.OutpostTile;
import digital.hiett.nationsofwar.outpost.OutpostTileType;
import digital.hiett.nationsofwar.screens.presets.IsometricRenderingScreen;
import digital.hiett.nationsofwar.util.ImageTextSkin;
import digital.hiett.nationsofwar.shared.packets.Packet;

import java.util.Random;

public class OutpostScreen extends IsometricRenderingScreen {

    public OutpostScreen() {
        super(1, 50, 50, Color.BLUE);

        OutpostMap map = Main.getInstance().getLocalPlayer().getOutpostMap();

        // Load the map into sprites.
        for (int z = 0; z < map.getMapWidth(); z++) {
            for (int x = 0; x < map.getMapHeight(); x++) {
                getBaseLayer()[x][z] = new Sprite(map.getBaseMap()[x][z].getTexture());
                getBaseLayer()[x][z].setPosition(x * getTileSize(), z * getTileSize());
                getBaseLayer()[x][z].setSize(getTileSize(), getTileSize());
            }
        }

        for (int i = 0; i < 8; i++) {
            for (int ix = 0; ix < 8; ix++) {
                OutpostTile outpostTile = OutpostTile.values()[new Random().nextInt(OutpostTile.values().length)];

                if (outpostTile.getOutpostTileType() == OutpostTileType.ITEM)
                    createNewLayer(outpostTile.getTexture(), 21 + i, 21 + ix, outpostTile.getRecommendedScale());
            }
        }
    }

    @Override
    public void createHud(Stage hudStage) {
        Skin transparent = new ImageTextSkin("misc/transparent.png").getSkin();

        Table leftButtons = new Table();
        leftButtons.setFillParent(false);
        leftButtons.setPosition(150, Gdx.graphics.getHeight() - 90);
        hudStage.addActor(leftButtons);

        TextButton toBattle = new TextButton("To Battle!", new ImageTextSkin("buttons/button.png").getSkin());
        leftButtons.add(toBattle);

        toBattle.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Enter matchmaking!
                Main.getInstance().getNetworkingManager().sendPacket(Packet.ENTER_MATCHMAKING.build("SOLO"));

                Main.getInstance().setScreen(new MatchmakingScreen());
            }
        });


        Table table = new Table();
        table.setFillParent(false);
        table.setPosition(150, 50);
        hudStage.addActor(table);

        TextButton coins = new TextButton(" ", new ImageTextSkin("misc/dollar.png").getSkin());
        table.add(coins);

        TextButton money = new TextButton("$" + Main.getInstance().getLocalPlayer().getCurrencyAmount(OutpostCurrency.GOLD), transparent);
        table.add(money);

        table.add(new TextButton("  ", transparent));

        TextButton wood = new TextButton(" ", new ImageTextSkin("misc/wood.png").getSkin());
        table.add(wood);

        TextButton woodAmount = new TextButton("" + Main.getInstance().getLocalPlayer().getCurrencyAmount(OutpostCurrency.WOOD), transparent);
        table.add(woodAmount);
    }

    @Override
    public void drawBatch(float delta) {
        OutpostMap map = Main.getInstance().getLocalPlayer().getOutpostMap();

        for (int z = 0; z < map.getMapWidth(); z++)
            for (int x = 0; x < map.getMapHeight(); x++)
                getBaseLayer()[x][z].draw(getBatch());
    }

    @Override
    public boolean tileTouched(int x, int y) {
        return true;
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) {
        Ray pickRay = getCamera().getPickRay(x, y);
        Intersector.intersectRayPlane(pickRay, getXzPlane(), getCurr());

        if (!(getLast().x == -1 && getLast().y == -1 && getLast().z == -1)) {
            pickRay = getCamera().getPickRay(getLast().x, getLast().y);
            Intersector.intersectRayPlane(pickRay, getXzPlane(), getDelta());
            getDelta().sub(getCurr());

            getCamera().position.add(getDelta().x, getDelta().y, getDelta().z);

            if (getCamera().position.x < 0)
                getCamera().position.x = 0;

            if (getCamera().position.y < 0)
                getCamera().position.y = 0;

            if (getCamera().position.z < 0)
                getCamera().position.z = 0;
        }
        getLast().set(x, y, 0);

        return false;
    }

}
