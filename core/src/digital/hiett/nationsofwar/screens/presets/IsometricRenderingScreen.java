package digital.hiett.nationsofwar.screens.presets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.ArrayList;
import java.util.List;

public abstract class IsometricRenderingScreen implements Screen, InputProcessor {

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Stage hudStage;
    private Sprite[][] baseLayer;
    private List<Sprite> layeredSprites;
    private Matrix4 matrix;
    private Plane xzPlane;
    private Vector3 intersection, curr, last, delta;
    private Sprite lastSelectedTile;
    private int tileSize, mapWidth, mapHeight;
    private Color selectionColor;

    public IsometricRenderingScreen(int tileSize, int mapWidth, int mapHeight, Color selectionColor) {
        this.selectionColor = selectionColor;
        this.tileSize = tileSize;
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;

        camera = new OrthographicCamera(10, 10 * (Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth()));
        camera.position.set(mapWidth / 2, 5, mapHeight / 2);
        camera.direction.set(-1, -1, -1);
        camera.near = 1;
        camera.far = 100;
        matrix = new Matrix4();
        matrix.setToRotation(new Vector3(1, 0, 0), 90);

        batch = new SpriteBatch();
        layeredSprites = new ArrayList<>();
        baseLayer = new Sprite[mapWidth][mapHeight];
        intersection = new Vector3();
        curr = new Vector3();
        last = new Vector3(-1, -1, -1);
        delta = new Vector3();
        xzPlane = new Plane(new Vector3(0, 1, 0), 0);
        hudStage = new Stage();
        lastSelectedTile = null;

        createHud(hudStage);

        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(this);
        inputMultiplexer.addProcessor(hudStage);

        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    public void createNewRawLayer(Texture texture, int x, int y) {
        Sprite sprite = new Sprite(texture);
        sprite.setSize(tileSize, tileSize);
        sprite.setPosition(x * tileSize, y * tileSize);

        layeredSprites.add(sprite);
    }

    public void createNewLayer(Texture texture, int x, int y, int scaleX, int scaleY) {
        Sprite sprite = new Sprite(texture);
        sprite.setSize(tileSize * scaleX, tileSize * scaleY + 1);
        sprite.setPosition(x * tileSize - 0.2f, y * tileSize - 0.8f);

        sprite.setOriginCenter();
        sprite.rotate90(false);
        sprite.rotate90(false);
        sprite.rotate(315);
        sprite.flip(true, false);

        layeredSprites.add(sprite);
    }

    public void createNewLayer(Texture texture, int x, int y) {
        createNewLayer(texture, x, y, 1, 0);
    }

    public void createNewLayer(Texture texture, int x, int y, int scale) {
        createNewLayer(texture, x, y, scale, scale);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor((float) 52 / 255, (float) 152 / 255, (float) 219 / 255, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();

        batch.setProjectionMatrix(camera.combined);
        batch.setTransformMatrix(matrix);
        batch.begin();

        drawBatch(delta);

        layeredSprites.forEach(sprite -> sprite.draw(batch));

        drawLast(delta);

        batch.end();

        checkTileTouched();

        hudStage.act(delta);
        hudStage.draw();

        drawAfter(delta);
    }

    @Override
    public void resize(int width, int height) {
        hudStage.getViewport().setScreenSize(width, height);
        hudStage.getViewport().setWorldSize(width, height);
        camera.viewportWidth = 10;
        camera.viewportHeight = 10 * (Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth());
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        last.set(-1, -1, -1);

        return false;
    }

    @Override
    public void show() {
    }

    public void drawAfter(float delta) {
    }

    private void checkTileTouched() {
        if (!Gdx.input.justTouched())
            return;

        Ray pickRay = camera.getPickRay(Gdx.input.getX(), Gdx.input.getY());
        Intersector.intersectRayPlane(pickRay, xzPlane, intersection);
        int x = (int) intersection.x;
        int z = (int) intersection.z;

        if (x >= 0 && x < mapHeight && z >= 0 && z < mapWidth && tileTouched(x, z)) {
            if (lastSelectedTile != null)
                lastSelectedTile.setColor(1, 1, 1, 1);

            Sprite sprite = baseLayer[x][z];
            sprite.setColor(selectionColor);
            lastSelectedTile = sprite;
        }
    }

    public Color getSelectionColor() {
        return selectionColor;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public int getTileSize() {
        return tileSize;
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public List<Sprite> getLayeredSprites() {
        return layeredSprites;
    }

    public Matrix4 getMatrix() {
        return matrix;
    }

    public Plane getXzPlane() {
        return xzPlane;
    }

    public Sprite getLastSelectedTile() {
        return lastSelectedTile;
    }

    public Sprite[][] getBaseLayer() {
        return baseLayer;
    }

    public Stage getHudStage() {
        return hudStage;
    }

    public Vector3 getCurr() {
        return curr;
    }

    public Vector3 getDelta() {
        return delta;
    }

    public Vector3 getIntersection() {
        return intersection;
    }

    public Vector3 getLast() {
        return last;
    }

    public abstract void createHud(Stage hudStage);

    public abstract void drawBatch(float delta);

    public abstract boolean tileTouched(int x, int y);

    public void drawLast(float delta) {
    }

}
