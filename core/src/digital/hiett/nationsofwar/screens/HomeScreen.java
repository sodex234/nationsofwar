package digital.hiett.nationsofwar.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.util.ImageTextSkin;
import digital.hiett.nationsofwar.util.TextFieldSkin;

public class HomeScreen implements Screen {

    private Stage stage;
    private SpriteBatch spriteBatch;

    public HomeScreen() {
        spriteBatch = new SpriteBatch();
        stage = new Stage();

        Skin skin = new ImageTextSkin("buttons/button.png").getSkin();

        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        Image logo = new Image(new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("logos/logo.png")))), Scaling.fit);
        logo.setWidth(0.1f);
        logo.setHeight(0.1f);
        table.add(logo).pad(100);

        table.row();

        TextField field = new TextField("Username", new TextFieldSkin().getStyle());
        table.add(field).width(Gdx.graphics.getWidth() - 250);

        table.row();

        TextField fieldPassword = new TextField("Password", new TextFieldSkin().getStyle());
        table.add(fieldPassword).width(Gdx.graphics.getWidth() - 250);

        table.row();

        TextButton play = new TextButton("Play", skin);
        table.add(play);

        play.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Main.getInstance().setScreen(new OutpostScreen());
            }
        });

        table.row();

        TextButton exit = new TextButton("Exit", skin);
        exit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });
        table.add(exit).spaceBottom(10);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}
