package digital.hiett.nationsofwar.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import digital.hiett.nationsofwar.Main;
import digital.hiett.nationsofwar.entities.DeployedUnit;
import digital.hiett.nationsofwar.entities.Unit;
import digital.hiett.nationsofwar.entities.UnitInstance;
import digital.hiett.nationsofwar.outpost.OutpostTile;
import digital.hiett.nationsofwar.outpost.OutpostTileType;
import digital.hiett.nationsofwar.screens.presets.IsometricRenderingScreen;
import digital.hiett.nationsofwar.shared.packets.Packet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class GameScreen extends IsometricRenderingScreen {

    private List<Vector2> teamSquares, enemyTeamSquares;
    private List<DeployedUnit> enemyDeployedUnits, teamDeployedUnits;
    private Label countdownTimerLabel;
    private ShapeRenderer shapeRenderer;
    private long startTime, attackLineDrawTime;
    private Vector2 attackLineA, attackLineB;
    private boolean madeSelectionA;

    public GameScreen() {
        super(1, 20, 20, Color.RED);

        this.startTime = System.currentTimeMillis() + (30 * 1000);

        this.teamSquares = new ArrayList<>();
        this.enemyTeamSquares = new ArrayList<>();
        this.enemyDeployedUnits = new ArrayList<>();
        this.teamDeployedUnits = new ArrayList<>();

        this.attackLineA = null;
        this.attackLineB = null;

        this.madeSelectionA = false;

        this.shapeRenderer = new ShapeRenderer();

        getCamera().position.set(12.5f, 5, 14.5f);

        // Load the map into sprites.
        for (int z = 0; z < getMapWidth(); z++) {
            for (int x = 0; x < getMapHeight(); x++) {
                getBaseLayer()[x][z] = new Sprite(OutpostTile.GRASS.getTexture());
                getBaseLayer()[x][z].setPosition(x * getTileSize(), z * getTileSize());
                getBaseLayer()[x][z].setSize(getTileSize(), getTileSize());
            }
        }

        List<Vector2> usedSquares = new ArrayList<>();
        for (int i = 0; i < 5; i++)
            for (int ix = 0; ix < 9; ix++)
                usedSquares.add(new Vector2(5 + i, 5 + ix));

        for (int i = 0; i < 3; i++)
            for (int ix = 0; ix < 3; ix++) {
                createNewRawLayer(OutpostTile.PLACE_GOOD_GRASS.getTexture(), 6 + i, 10 + ix);
                teamSquares.add(new Vector2(6 + i, 10 + ix));
            }

        for (int i = 0; i < 3; i++)
            for (int ix = 0; ix < 3; ix++) {
                createNewRawLayer(OutpostTile.PLACE_BAD_GRASS.getTexture(), 6 + i, 6 + ix);
                enemyTeamSquares.add(new Vector2(6 + i, 6 + ix));
            }

        for (int i = 0; i < getMapWidth(); i++) {
            for (int ix = 0; ix < getMapHeight(); ix++) {
                OutpostTile outpostTile = OutpostTile.values()[new Random().nextInt(OutpostTile.values().length)];

                if (outpostTile.getOutpostTileType() == OutpostTileType.ITEM && new Random().nextBoolean()
                        && !usedSquares.contains(new Vector2(i, ix)))
                    createNewLayer(outpostTile.getTexture(), i, ix, outpostTile.getRecommendedScale());
            }
        }
    }

    @Override
    public void createHud(Stage hudStage) {
        Table leftButtons = new Table();
        leftButtons.setFillParent(false);
        leftButtons.setPosition(210, Gdx.graphics.getHeight() - 60);
        hudStage.addActor(leftButtons);

        leftButtons.add(new Label("Playing '" + Main.getInstance().getGameManager().getOtherName() + "'.",
                new Label.LabelStyle(new BitmapFont(Gdx.files.internal("main.fnt")), Color.BLACK)));

        countdownTimerLabel = new Label("30s", new Label.LabelStyle(new BitmapFont(Gdx.files.internal("main.fnt")), Color.BLACK));
        leftButtons.row();
        leftButtons.add(countdownTimerLabel);
    }

    @Override
    public void drawBatch(float delta) {
        for (int z = 0; z < getMapWidth(); z++)
            for (int x = 0; x < getMapHeight(); x++)
                getBaseLayer()[x][z].draw(getBatch());
    }

    @Override
    public boolean tileTouched(int x, int y) {
        Vector2 instance = new Vector2(x, y);

        boolean allowed = teamSquares.contains(instance) || enemyTeamSquares.contains(instance);

        if (teamSquares.contains(instance) && inEdit()) {
            // Place down the Unit here.

            DeployedUnit toRemove = null;
            for (DeployedUnit deployedUnit : teamDeployedUnits) {
                if (deployedUnit.getLocation().x == x && deployedUnit.getLocation().y == y) {
                    toRemove = deployedUnit;

                    break;
                }
            }

            if (toRemove == null) {
                UnitInstance unitInstance = UnitInstance.values()[new Random().nextInt(UnitInstance.values().length)];

                // Place the unit
                DeployedUnit newUnit = new DeployedUnit(unitInstance, new Vector2(x, y));
                teamDeployedUnits.add(newUnit);

                // Send the packet to the server telling it that we have just placed down a unit.
                Main.getInstance().getNetworkingManager().sendPacket(Packet.PLAYER_PLACE_UNIT
                        .build(unitInstance.ordinal() + "", x + "", y + "", newUnit.getUnitId() + ""));
            } else {
                // Remove the unit
                teamDeployedUnits.remove(toRemove);
                Main.getInstance().getNetworkingManager().sendPacket(Packet.PLAYER_PLACE_UNIT
                        .build("-1", x + "", y + "", toRemove.getUnitId() + ""));
            }
        } else if (!Main.getInstance().getGameManager().isThisPlayersTurn()) {
            return false; // They can't go if its not their go!
        } else if (Main.getInstance().getGameManager().isThisPlayersTurn() && allowed && locationContainsUnit(x, y)) {
            if (!madeSelectionA) {
                allowed = teamSquares.contains(instance);

                if (allowed) {
                    madeSelectionA = true;

                    DeployedUnit du = getLocationDeployedUnit(x, y);

                    // Send this packet
                    Main.getInstance().getNetworkingManager().sendPacket(Packet.PLAYER_SELECTION.build(x + "", y + "", du.getUnitId() + ""));
                }
            } else {
                allowed = enemyTeamSquares.contains(instance);

                if (allowed) {
                    madeSelectionA = false;

                    DeployedUnit du = getLocationDeployedUnit(x, y);

                    Main.getInstance().getNetworkingManager().sendPacket(Packet.PLAYER_SELECTION.build(x + "", y + "", du.getUnitId() + ""));
                }
            }
        }

        return allowed;
    }

    @Override
    public void drawLast(float delta) {
        // Draw the units
        enemyDeployedUnits.forEach(unit -> unit.getFrontSprite().draw(getBatch()));
        teamDeployedUnits.forEach(unit -> unit.getBackSprite().draw(getBatch()));

        // Update the countdown timer.
        if (startTime != -1) {
            long timeDifferenceLong = (startTime - System.currentTimeMillis()) / 1000;
            int timeDifference = Math.round(timeDifferenceLong); // Remove the decimal places.

            countdownTimerLabel.setText(timeDifference + "s");

            if (timeDifference <= 0) {
                // End the timer.
                startTime = -1;
                // TODO: make it so that this changes the entire UI element.
            }
        }
    }

    @Override
    public void drawAfter(float delta) {
        shapeRenderer.setProjectionMatrix(getCamera().combined);
        shapeRenderer.setTransformMatrix(getMatrix());

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        // Draw all of the health bars
        enemyDeployedUnits.forEach(unit -> unit.drawHealthBar(shapeRenderer));
        teamDeployedUnits.forEach(unit -> unit.drawHealthBar(shapeRenderer));

        shapeRenderer.end();

        if (attackLineA == null || attackLineB == null)
            return;

        if (System.currentTimeMillis() > attackLineDrawTime) {
            attackLineDrawTime = -1;
            attackLineA = null;
            attackLineB = null;

            return;
        }

        shapeRenderer.setProjectionMatrix(getCamera().combined);
        shapeRenderer.setTransformMatrix(getMatrix());

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(1, 0, 0, 1); // Red line
        shapeRenderer.line(attackLineA, attackLineB);
        shapeRenderer.end();

        checkAlive();
    }

    public void removeTeamUnit(DeployedUnit deployedUnit) {
        DeployedUnit toRemove = null;

        for(DeployedUnit deployedUnit1 : teamDeployedUnits) {
            if (deployedUnit.getUnitId().equals(deployedUnit1.getUnitId())) { // Check for any changes.
                toRemove = deployedUnit1;

                break;
            }
        }

        System.out.println("Removing team unit " + toRemove);

        if(toRemove != null)
            teamDeployedUnits.remove(toRemove);
    }

    public boolean inEdit() {
        return startTime != -1;
    }

    public List<DeployedUnit> getEnemyDeployedUnits() {
        return enemyDeployedUnits;
    }

    public List<DeployedUnit> getTeamDeployedUnits() {
        return teamDeployedUnits;
    }

    public void addEnemyDeployedUnit(int id, int x, int y, UUID uuid) {
        enemyDeployedUnits.add(new DeployedUnit(UnitInstance.values()[id], new Vector2(x, y), uuid));
    }

    public void removeEnemyDeployedUnit(int x, int y) {
        DeployedUnit toRemove = null;

        for (DeployedUnit deployedUnit : enemyDeployedUnits) {
            if (deployedUnit.getLocation().x == x && deployedUnit.getLocation().y == y) {
                toRemove = deployedUnit;

                break;
            }
        }

        System.out.println("Removing Enemy Deployed Unit: " + toRemove);

        if (toRemove != null)
            enemyDeployedUnits.remove(toRemove);
    }

    public boolean locationContainsUnit(int x, int y) {
        return getLocationDeployedUnit(x, y) != null;
    }

    public DeployedUnit getLocationDeployedUnit(int x, int y) {
        List<DeployedUnit> dUnits = new ArrayList<>(enemyDeployedUnits);
        dUnits.addAll(teamDeployedUnits);

        for (DeployedUnit deployedUnit : dUnits)
            if (deployedUnit.getLocation().x == x && deployedUnit.getLocation().y == y)
                return deployedUnit;

        return null;
    }

    public void removeUnit(UUID to) {
        DeployedUnit thisUnit = getUnitFromUUID(to);
        if(thisUnit == null)
            return;

        if(enemyDeployedUnits.contains(thisUnit))
            enemyDeployedUnits.remove(thisUnit);
        else if(teamDeployedUnits.contains(thisUnit))
            teamDeployedUnits.remove(thisUnit);
    }

    public void checkAlive() {
        if(enemyDeployedUnits.size() == 0 || teamDeployedUnits.size() == 0) {
            Main.getInstance().getNetworkingManager().sendPacket(Packet.GAME_OVER.build(""));

            Main.getInstance().setScreen(new GameOverScreen(enemyDeployedUnits.size() == 0));
        }
    }

    public DeployedUnit getUnitFromUUID(UUID uuid) {
        List<DeployedUnit> dUnits = new ArrayList<>(enemyDeployedUnits);
        dUnits.addAll(teamDeployedUnits);

        for (DeployedUnit deployedUnit : dUnits)
            if (deployedUnit.getUnitId().equals(uuid))
                return deployedUnit;

        return null;
    }

    public void attack(int fromX, int fromY, int toX, int toY) {
        this.attackLineA = new Vector2(fromX, fromY);
        this.attackLineB = new Vector2(toX, toY);

        this.attackLineDrawTime = System.currentTimeMillis() + 2000; // Two seconds
    }

}
