package digital.hiett.nationsofwar.entities;

import digital.hiett.nationsofwar.entities.units.Blitzer;
import digital.hiett.nationsofwar.entities.units.RaiderWarrior;

public enum UnitInstance {

    RADIER_WARRIOR(new RaiderWarrior()),
    BLITZER(new Blitzer());

    private Unit unit;

    UnitInstance(Unit unit) {
        this.unit = unit;
    }

    public Unit getUnitInstance() {
        return unit;
    }

}
