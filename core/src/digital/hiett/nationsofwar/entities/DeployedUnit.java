package digital.hiett.nationsofwar.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.UUID;

public class DeployedUnit {

    private static final float HEALTH_BAR_WIDTH = 0.1f, HEALTH_BAR_HEIGHT = 0.5f;

    private UnitInstance unit;
    private Sprite frontSprite, backSprite;
    private int health;
    private Vector2 location;
    private UUID unitId;

    public DeployedUnit(UnitInstance unit, Vector2 location, UUID unitId) {
        this.unit = unit;
        this.location = location;
        this.unitId = unitId;

        this.health = unit.getUnitInstance().getHealth();

        this.frontSprite = createSprite(unit.getUnitInstance().getFrontTexture());
        this.backSprite = createSprite(unit.getUnitInstance().getBackTexture());
    }

    public DeployedUnit(UnitInstance unitInstance,  Vector2 location) {
        this(unitInstance, location, UUID.randomUUID());
    }

    private Sprite createSprite(Texture texture) {
        Sprite toCreate = new Sprite(texture);
        toCreate.setSize(1, 2);
        toCreate.setPosition(location.x - 0.2f, location.y - 0.8f);
        toCreate.setOriginCenter();
        toCreate.rotate90(false);
        toCreate.rotate90(false);
        toCreate.rotate(315);
        toCreate.flip(true, false);

        return toCreate;
    }

    public UUID getUnitId() {
        return this.unitId;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHealth() {
        return health;
    }

    public Vector2 getLocation() {
        return location;
    }

    public void setLocation(Vector2 location) {
        this.location = location;
        frontSprite.setPosition(location.x - 0.2f, location.y - 0.8f);
        backSprite.setPosition(location.x - 0.2f, location.y - 0.8f);
    }

    public UnitInstance getUnit() {
        return unit;
    }

    public Unit getUnitClass() {
        return unit.getUnitInstance();
    }

    public Sprite getBackSprite() {
        return backSprite;
    }

    public Sprite getFrontSprite() {
        return frontSprite;
    }

    public void drawHealthBar(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(1f, 0f, 0f, 1f);
        shapeRenderer.rect(getLocation().x, getLocation().y, HEALTH_BAR_WIDTH, HEALTH_BAR_HEIGHT);

        // Calculate how much of this needs to be filled based on health amount.
        float percentageOfHealth = health * 100 / unit.getUnitInstance().getHealth();

        shapeRenderer.setColor(0f, 1f, 0f, 1f);
        shapeRenderer.rect(getLocation().x, getLocation().y, HEALTH_BAR_WIDTH, HEALTH_BAR_HEIGHT * (percentageOfHealth / 100));
    }

    public boolean isDead() {
        return health <= 0;
    }

    public boolean isAlive() {
        return !isDead();
    }

    public void damage(int amount) {
        if(this.health - amount <= 0) {
            // They have just died!
            this.health = 0;

            return;
        }

        this.health -= amount;
    }

}
