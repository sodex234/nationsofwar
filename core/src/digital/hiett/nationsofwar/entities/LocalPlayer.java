package digital.hiett.nationsofwar.entities;

import digital.hiett.nationsofwar.outpost.OutpostCurrency;
import digital.hiett.nationsofwar.outpost.OutpostMap;

import java.util.*;

public class LocalPlayer {

    private OutpostMap outpostMap;
    private Map<OutpostCurrency, Integer> currencies;
    private List<UnitInstance> unlockedUnits, usedUnits;
    private UUID uuid;
    private String name;
    private int queueSize;

    public LocalPlayer() {
        this.outpostMap = new OutpostMap();
        this.currencies = new HashMap<>();
        this.unlockedUnits = new ArrayList<>();
        this.usedUnits = new ArrayList<>();

        this.queueSize = 0;

        this.unlockedUnits.add(UnitInstance.RADIER_WARRIOR);

        Arrays.asList(OutpostCurrency.values()).forEach(currency -> this.currencies.put(currency, 100));
    }

    public int getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(int queueSize) {
        this.queueSize = queueSize;
    }

    public List<UnitInstance> getUnlockedUnits() {
        return unlockedUnits;
    }

    public List<UnitInstance> getUsedUnits() {
        return usedUnits;
    }

    public OutpostMap getOutpostMap() {
        return outpostMap;
    }

    public Map<OutpostCurrency, Integer> getCurrencies() {
        return currencies;
    }

    public int getCurrencyAmount(OutpostCurrency outpostCurrency) {
        return currencies.getOrDefault(outpostCurrency, 0);
    }

    public void addCurrency(OutpostCurrency outpostCurrency, int amount) {
        setCurrency(outpostCurrency, getCurrencyAmount(outpostCurrency) + amount);
    }

    public void subtractCurrency(OutpostCurrency outpostCurrency, int amount) {
        setCurrency(outpostCurrency, getCurrencyAmount(outpostCurrency) - amount);
    }

    public void setCurrency(OutpostCurrency outpostCurrency, int amount) {
        this.currencies.put(outpostCurrency, amount);
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

}
