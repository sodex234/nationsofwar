package digital.hiett.nationsofwar.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import digital.hiett.nationsofwar.Main;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class DesktopLauncher {

    private JFrame launcherWindow, gameWindow;

    public DesktopLauncher() {
        this.launcherWindow = new JFrame("Nations of War Launcher");
        this.launcherWindow.setSize(300, 500);
        this.launcherWindow.setResizable(false);

        this.launcherWindow.setBackground(Color.DARK_GRAY);
        this.launcherWindow.getContentPane().setBackground(Color.DARK_GRAY);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(new JLabel("<html><h1 size=32>Nations of War Launcher</h1></html>"));

        JTextField ipField = new JTextField("localhost");
        panel.add(ipField);

        JTextField widthField = new JTextField("1280");
        panel.add(widthField);

        JTextField heightField = new JTextField("720");
        panel.add(heightField);

        JTextField usernameField = new JTextField("Username");
        panel.add(usernameField);

        JPasswordField passwordField = new JPasswordField("Password");
        panel.add(passwordField);

        JButton startServer = new JButton("Start 'localhost' Server (WINDOWS ONLY)");
        startServer.addActionListener((e) -> {
            try {
                Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"java -jar server.jar\"");
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(null, "For some reason, you couldn't start the server.");

                e1.printStackTrace();
            }
        });
        panel.add(startServer);

        JButton startGameButton = new JButton("Start Game");
        startGameButton.addActionListener((e) -> {
            // Launch the game

            LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
            config.title = "Nations of War";
            config.width = Integer.parseInt(widthField.getText());
            config.height = Integer.parseInt(heightField.getText());

            new LwjglApplication(new Main(ipField.getText()), config);
        });
        panel.add(startGameButton);

        this.launcherWindow.getContentPane().add(panel);

        this.launcherWindow.setVisible(true);
        this.launcherWindow.setLocationRelativeTo(null);
        this.launcherWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.launcherWindow.revalidate();
        this.launcherWindow.repaint();
    }

    // Static //

    public static void main(String[] arg) {
        new DesktopLauncher();

//
//        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//        config.title = "Nations of War";
//        config.width = 1280;
//        config.height = 720;
//        String multiplayerIp = JOptionPane.showInputDialog("Enter Multiplayer IP");
////        config.
////        new LwjglApplication(new Main(multiplayerIp), config);
//
//        JFrame frame = new JFrame("TEST");
//
//        Canvas canvas = new Canvas();
//
//        frame.add(canvas);
//
//        frame.setSize(config.width, config.height);
//        frame.setVisible(true);
//        frame.setLocationRelativeTo(null);
//
//        new LwjglApplication(new Main(multiplayerIp), config, canvas);
    }

}
